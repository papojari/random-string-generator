#!/bin/sh

# Generate a random string from /dev/urandom
head -2 /dev/urandom |

# Filter for base64 alphabet
base64 -w 0 |

# Filter $RandomBase64 for a-z
sed 's/[^a-z]//g' |

# Print first character
cut -c 1-16
