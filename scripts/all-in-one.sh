#!/bin/sh

# Generate a random string from /dev/urandom
RANDOM=$(head -2 /dev/urandom)

# Filter for base64 alphabet
RANDOM_BASE64=$(printf "%s\n" "${RANDOM}" | base64 -w 0)

if [[ "$1" == "-a-z" ]]
then
	# Filter $RandomBase64 for a-z
	RANDOM_LETTERS_OR_NUMBERS=$(printf "%s\n" "${RANDOM_BASE64}" | sed 's/[^a-z]//g')
elif [[ "$1" == "-0-9" ]]
then
	# Filter $RandomBase64 for a-z
	RANDOM_LETTERS_OR_NUMBERS=$(printf "%s\n" "${RANDOM_BASE64}" | sed 's/[^0-9]//g')
else
	echo "all-in-one.sh -[a-z/0-9] -[length in decimal]"
fi

# Print only how many characters were asked for
printf "%s\n" "${RANDOM_LETTERS_OR_NUMBERS}" | cut -c 1"$2"