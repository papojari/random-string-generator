# random-string-generator

With the shell scripts in `scripts` you can generate a random string of characters from *a*-*z* or *0*-*9*.

## License

[View the license](UNLICENSE)